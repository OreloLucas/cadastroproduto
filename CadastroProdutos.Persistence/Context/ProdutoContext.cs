﻿using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Persistence.Mapping;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Persistence.Context
{
    public class ProdutoContext : DbContext
    {
        public bool lazyload = false;

        public ProdutoContext(DbContextOptions<ProdutoContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ProdutoMapping());
            builder.ApplyConfiguration(new MovimentacaoMapping());
            builder.ApplyConfiguration(new UsuarioMapping());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseLazyLoadingProxies(lazyload);

        }

        public DbSet<Produto> Produto { get; set; } 
        public DbSet<Movimentacao> Movimentacao { get; set; } 
        public DbSet<Usuario> Usuario { get; set; } 

    }
}
