﻿using CadastroProdutos.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Persistence.Mapping
{
    public class MovimentacaoMapping : IEntityTypeConfiguration<Movimentacao>
    {
        public void Configure(EntityTypeBuilder<Movimentacao> builder)
        {
            builder.ToTable("movimentacao");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.IdProduto).HasColumnName("idproduto").HasColumnType("int");
            builder.Property(x => x.Quantidade).HasColumnName("quantidade").HasColumnType("int");
            builder.Property(x => x.Data).HasColumnName("data").HasColumnType("DateTime");

            builder.HasOne(x => x.Produto).WithMany().HasForeignKey(x=>x.IdProduto );
        }
    }
}
