﻿using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Domain.Interfaces;
using CadastroProdutos.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Persistence.Repositories
{
    public class MovimentacaoRepository : IMovimentacaoRepository
    {
        private readonly ProdutoContext _context;
        public MovimentacaoRepository(ProdutoContext context)
        {
            _context = context;
        }

        public Movimentacao Movimenta(Movimentacao movimentacao)
        {
            try
            {
                _context.Movimentacao.Add(movimentacao);
                _context.SaveChanges();
                return movimentacao;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ICollection<Movimentacao>? Lista(int pagina)
        {
            try
            {
                _context.lazyload = false;
                return _context.Movimentacao.Skip(10 * (pagina - 1)).Take(10).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
