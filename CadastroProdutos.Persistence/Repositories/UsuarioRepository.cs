﻿using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Domain.Interfaces;
using CadastroProdutos.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Persistence.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly ProdutoContext _context;
        public UsuarioRepository(ProdutoContext context)
        {
            _context = context;
        }

        public Usuario Adiciona(Usuario usuario)
        {
            try
            {
                _context.Usuario.Add(usuario);
                _context.SaveChanges();
                return usuario;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Usuario? Get(string email, string password) => _context.Usuario.Where(x => x.Email.Equals(email) && x.Password.Equals(password)).FirstOrDefault();

        public Usuario? GetByEmail(string email) => _context.Usuario.Where(x => x.Email.Equals(email)).FirstOrDefault();

        public ICollection<Usuario>? Lista(int pagina)
        {
            try
            {
                _context.lazyload = false;
                return _context.Usuario.Skip(10 * (pagina - 1)).Take(10).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
