﻿using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Domain.Interfaces;
using CadastroProdutos.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Persistence.Repositories
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly ProdutoContext _context;
        public ProdutoRepository(ProdutoContext context)
        {
            _context = context;
        }

        public Produto Adiciona(Produto produto)
        {
            try
            {
                _context.Produto.Add(produto);
                _context.SaveChanges();
                return produto;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Produto Get(int idProduto) => _context.Produto.Where(x => x.Id == idProduto).FirstOrDefault();

        public ICollection<Produto>? Lista(int pagina)
        {
            try
            {
                _context.lazyload = false;
                return _context.Produto.Skip(10 * (pagina - 1)).Take(10).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
