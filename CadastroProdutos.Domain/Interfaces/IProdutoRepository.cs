﻿using CadastroProdutos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Domain.Interfaces
{
    public interface IProdutoRepository
    {
        ICollection<Produto>? Lista(int pagina); 
        Produto Adiciona(Produto produto);
        Produto Get(int idProduto);
    }
}
