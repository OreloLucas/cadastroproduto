﻿using CadastroProdutos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Domain.Interfaces
{
    public interface IMovimentacaoRepository
    {
        ICollection<Movimentacao>? Lista(int pagina);
        Movimentacao Movimenta(Movimentacao produto);
    }
}
