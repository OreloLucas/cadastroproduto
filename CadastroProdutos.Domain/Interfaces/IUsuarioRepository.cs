﻿using CadastroProdutos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Domain.Interfaces
{
    public interface IUsuarioRepository
    { 
        Usuario Adiciona(Usuario produto); 
        ICollection<Usuario>? Lista(int pagina);
        Usuario? Get(string email, string password);
        Usuario? GetByEmail(string email);
    }
}
