﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Domain.Enum
{
    public enum TipoMovimentacao : byte
    {
        Saida = 0,
        Entrada = 1,
    }
}
