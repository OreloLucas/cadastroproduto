﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Domain.Entities
{
    public class Movimentacao
    {
        public int Id { get; set; }
        public int IdProduto { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }
        public virtual Produto? Produto { get; set; }
    }
}
