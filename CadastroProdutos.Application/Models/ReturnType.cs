﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Application.Models
{
    public class ReturnType<T>
    {
        public string Mensagem { get; set; }
        public bool Sucesso { get; set; }
        public T obj { get; set; }
    }
}
