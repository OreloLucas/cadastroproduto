﻿using CadastroProdutos.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Application.Models
{
    public class MovimentacaoViewModel
    {
        public int IdProduto { get; set; }
        public int Quantidade { get; set; }
        public TipoMovimentacao TpMovimentacao { get; set; }
    }
}
