﻿using CadastroProdutos.Application.Models;
using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Domain.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using CadastroProdutos.Domain;

namespace CadastroProdutos.Application.Interface
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }
        public ReturnType<Usuario> Adicionar(UsuarioViewModel usuario)
        {
            if (!usuario.Password.Equals(usuario.ReTryPassword)) return new ReturnType<Usuario>() { Sucesso = false, Mensagem = "Senhas diferentes" };
            if (_usuarioRepository.GetByEmail(usuario.Email) is not null) return new ReturnType<Usuario>() { Sucesso = false, Mensagem = "e-mail já cadastrado" };

            return new ReturnType<Usuario>()
            {
                obj = _usuarioRepository.Adiciona(new Usuario()
                {
                    Nome = usuario.Nome,
                    Password = EncodeTo64(usuario.Password),
                    Email = usuario.Email
                })
            };
        }

        private string EncodeTo64(string toEncode) => System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode));

        private string DecodeFrom64(string encodedData) => System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(encodedData));

        public ReturnType<ICollection<Usuario>?> Lista(int pagina)
        => new ReturnType<ICollection<Usuario>?>()
        {
            obj = _usuarioRepository.Lista(pagina).Select(x => new Usuario()
            {
                Nome = x.Nome,
                Email = x.Email,
                Password = "",
                Id = x.Id,
                //Password = DecodeFrom64(x.Password)
            }).ToList(),
            Sucesso = true
        };

        public ReturnType<string> Login(UsuarioViewModel user)
        {
            var usuario = _usuarioRepository.Get(user.Email, EncodeTo64(user.Password));
            if (usuario is null) return new ReturnType<string>() { Mensagem = "e-mail ou senha inválidos", Sucesso = false };
            var token = GenerateToken(usuario.Email, usuario.Nome);

            return String.IsNullOrEmpty(token) ?
                new ReturnType<string>() { Mensagem = "erro ao gerar token", Sucesso = false } :
                new ReturnType<string>() { Mensagem = "ok", obj = token, Sucesso = true };
        }

        private string GenerateToken(string email, string nome)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(TokenOptions.TokenSecurityKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, nome),
                    new Claim(ClaimTypes.Email, email),
                    }),
                    Expires = DateTime.UtcNow.AddHours(2),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
            catch (Exception)
            {
                return "";
            };
        }
    }
}
