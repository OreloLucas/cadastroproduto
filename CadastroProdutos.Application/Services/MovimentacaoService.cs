﻿using CadastroProdutos.Application.Models;
using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Domain.Enum;
using CadastroProdutos.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Application.Interface
{
    public class MovimentacaoService : IMovimentacaoService
    {
        private readonly IMovimentacaoRepository _movimentacaoRepository;
        private readonly IProdutoRepository _produtoRepository;

        public MovimentacaoService(IMovimentacaoRepository movimentacaoRepository, IProdutoRepository produtoRepository)
        {
            _movimentacaoRepository = movimentacaoRepository;
            _produtoRepository = produtoRepository;
        }

        public ReturnType<Movimentacao> Movimenta(MovimentacaoViewModel movimentacao)
        {
            if (!Enum.IsDefined(typeof(TipoMovimentacao), movimentacao.TpMovimentacao))
                return new ReturnType<Movimentacao>() { Sucesso = false, Mensagem = "Tipo de movimentação indefinido" };

            var produto = _produtoRepository.Get(movimentacao.IdProduto);
            if (produto is null) return new ReturnType<Movimentacao>() { Sucesso = false, Mensagem = "Produto não localizado" };

            var quantidade = movimentacao.TpMovimentacao.Equals(TipoMovimentacao.Saida) ?
                   Math.Abs(movimentacao.Quantidade) * -1 : Math.Abs(movimentacao.Quantidade);

            produto.Quantidade += quantidade;
            if (produto.Quantidade < 0) return new ReturnType<Movimentacao>() { Sucesso = false, Mensagem = "Quantidade restante não pode ser negativa!" };

            var mov = new Movimentacao()
            {
                Data = DateTime.Now,
                IdProduto = movimentacao.IdProduto,
                Quantidade = quantidade,
                Produto = produto,
            };

            _movimentacaoRepository.Movimenta(mov);

            return new ReturnType<Movimentacao>()
            {
                obj = mov,
                Sucesso = true,
                Mensagem = "ok"
            };
        }

        public ReturnType<ICollection<Movimentacao>?> Lista(int pagina) => new ReturnType<ICollection<Movimentacao>?>()
        {
            obj = _movimentacaoRepository.Lista(pagina),
            Sucesso = true
        };
    }
}
