﻿using CadastroProdutos.Application.Models;
using CadastroProdutos.Domain.Entities;
using CadastroProdutos.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Application.Interface
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepository _produtoRepository;

        public ProdutoService(IProdutoRepository produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        public ReturnType<Produto> Adicionar(ProdutoViewModel produto)
            => new ReturnType<Produto>()
            {
                Sucesso = true,
                obj = _produtoRepository.Adiciona(new Produto()
                {
                    Nome = produto.Nome,
                    Quantidade = produto.Quantidade,
                })
            };

        public ReturnType<ICollection<Produto>?> Lista(int pagina) => new ReturnType<ICollection<Produto>?>()
        {
            obj = _produtoRepository.Lista(pagina),
            Sucesso = true
        };
    }
}
