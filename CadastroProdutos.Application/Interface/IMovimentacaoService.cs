﻿using CadastroProdutos.Application.Models;
using CadastroProdutos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Application.Interface
{
    public interface IMovimentacaoService
    {
        ReturnType<ICollection<Movimentacao>?> Lista(int pagina);
        ReturnType<Movimentacao> Movimenta(MovimentacaoViewModel produto);
    }
}
