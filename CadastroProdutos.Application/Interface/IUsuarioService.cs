﻿using CadastroProdutos.Application.Models;
using CadastroProdutos.Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProdutos.Application.Interface
{
    public interface IUsuarioService
    {
        ReturnType<Usuario> Adicionar(UsuarioViewModel produto);
        ReturnType<ICollection<Usuario>?> Lista(int pagina);

        ReturnType<string> Login(UsuarioViewModel user);
    }
}
