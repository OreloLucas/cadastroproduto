﻿using CadastroProdutos.Application.Interface;
using CadastroProdutos.Application.Models;
using Microsoft.AspNetCore.Mvc; 
using Microsoft.AspNetCore.Authorization;

namespace CadastroProdutos.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoService _produtoService;
        public ProdutoController(IProdutoService produtoService)
        {
            _produtoService = produtoService;
        }

        [HttpGet, Route("Lista/{pagina?}")]
        public IActionResult Lista(int pagina = 1)
        {
            var result = _produtoService.Lista(pagina);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPut, Route("Novo/")]
        public IActionResult Novo(ProdutoViewModel produto)
        {
            var result = _produtoService.Adicionar(produto);
            return result.Sucesso ? Ok(result.obj) : BadRequest(result);
        }

    }
}
