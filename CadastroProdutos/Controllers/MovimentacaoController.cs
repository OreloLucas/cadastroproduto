﻿using CadastroProdutos.Application.Interface;
using CadastroProdutos.Application.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace CadastroProdutos.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class MovimentacaoController : ControllerBase
    {
        private readonly IMovimentacaoService _movimentacaoService;
            
        public MovimentacaoController(IMovimentacaoService movimentacaoService)
        {
            _movimentacaoService = movimentacaoService;
        }

        [HttpGet, Route("Lista/{pagina?}")]
        public IActionResult Lista(int pagina = 1)
        {
            var result = _movimentacaoService.Lista(pagina);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Movimenta/")]
        public IActionResult Movimenta(MovimentacaoViewModel movimentacao)
        {
            var result = _movimentacaoService.Movimenta(movimentacao);
            return result.Sucesso ? Ok(result.obj) : BadRequest(result);
        }
 

    }
}
