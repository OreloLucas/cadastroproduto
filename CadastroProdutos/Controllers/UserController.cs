﻿using CadastroProdutos.Application.Interface;
using CadastroProdutos.Application.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace CadastroProdutos.WebApi.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;
        public UserController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [HttpPost("login"), AllowAnonymous]

        public IActionResult login(UsuarioViewModel usuario)
        {
            var result = _usuarioService.Login(usuario);
            return result.Sucesso ? Ok(result.obj) : BadRequest(result);
        }

        [HttpGet("lista/{pagina?}"), Authorize]
        public IActionResult Lista(int pagina = 1)
        {
            var result = _usuarioService.Lista(pagina);
            return result.Sucesso ? Ok(result.obj) : BadRequest(result);
        }

        [HttpPut, Route("Novo/"), Authorize]
        public IActionResult Novo(UsuarioViewModel usuario)
        {
            var result = _usuarioService.Adicionar(usuario);
            return result.Sucesso ? Ok(result.obj) : BadRequest(result);
        }
    }
}
